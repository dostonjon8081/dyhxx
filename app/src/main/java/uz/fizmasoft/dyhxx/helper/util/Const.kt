package uz.fizmasoft.dyhxx.helper.util

/**
 * preference
 */
const val CONST_PREF = "CONST_PREF"
const val PREF_LANG_KEY="PREF_LANG_KEY"
const val PREF_TOKEN_KEY="PREF_TOKEN_KEY"
const val PREF_USER_ID_KEY="PREF_USER_ID_KEY"

const val FIRST_INIT = "FIRST_INIT"

///**
// * URL
// */
//const val BASE_URL = "https://api.fizmasoft.uz/dyhxxuz/"
const val BASE_URL = "https://api.autoinfo.uz/"
//const val TELEGRAM_AUTH_URL = "https://t.me/dyhxxuz_auth_bot"
//const val TELEGRAM_AUTH_URL = "tg://resolve?domain=dyhxxuz_bot&start=android"
//const val TELEGRAM_AUTH_URL = "tg://resolve?domain=autoinfouz_bot&start=android"
const val TELEGRAM_AUTH_URL = "tg://resolve?domain=autoinfouz_bot&start=325f52f7946f4737aeba940a8452648b"
const val TELEGRAM_FEEDBACK_URL = "tg://resolve?domain=autoinfouz_feedback_bot"
const val TELEGRAM_FEEDBACK_URL_CHROME = "https://t.me/@autoinfouz_feedback_bot"

/**
 * permission request code
 */

const val STORAGE_REQUEST_CODE = 1001
const val IN_APP_UPDATE_REQUEST_CODE = 4040
const val IMG_REQUEST_CODE = 4041
